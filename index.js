//
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response)=>{
	return response.json();

})

.then((result)=>{
	
	console.log(result);
	return showPost(result)

});

const showPost =(posts) =>{
	let postEntries = ''
	posts.forEach((post)=>{

		postEntries +=`
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
			</div> `
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

	document.querySelector('#form-add-post').addEventListener('submit',(event)=>{

		event.preventDefault();
		fetch('https://jsonplaceholder.typicode.com/posts',{
			method: 'POST',// get/ delete/put
			body:JSON.stringify({
				title: document.querySelector('#txt-title').value,
				body: document.querySelector('#txt-body').value,
				userId: 101
			}),
			headers: {'Content-type' : 'application/json ; charset=UTF-8'}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);
			alert('Succesfully Added.')
			document.querySelector('#txt-title').value = null;
			document.querySelector('#txt-body').value = null;
		})
	})
	//Edit Post
	const editPost = (id) =>{
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		document.querySelector('#txt-edit-id').value = id;
		document.querySelector('#txt-edit-title').value = title;
		document.querySelector('#txt-edit-body').value = body;
		document.querySelector('#btn-submit-update').removeAttribute('disable');
	}	

	document.querySelector('#form-edit-post').addEventListener('submit',(e)=>{
		e.preventDefault();
		fetch("https://jsonplaceholder.typicode.com/posts/1",{
			//specify what HTTP method to be used
			method: 'PUT',
			body:JSON.stringify({
				id: document.querySelector('#txt-edit-id').value,
				title: document.querySelector('#txt-edit-title').value,
				body: document.querySelector('#txt-edit-body').value,
				userId:1
			}),
			headers: {'Content-type' : 'application/json ; charset=UTF-8'}
		})
		.then((response)=> response.json())
		.then(result =>{

			console.log(result);
			alert('Successfully updated');

			//make the input boxes value null
			document.querySelector('#txt-edit-id').value = null;
			document.querySelector('#txt-edit-title').value = null;
			document.querySelector('#txt-edit-body').value = null;
			document.querySelector('#btn-submit-update').setAttribute('disable', true);
		})

	});

	//Delete post
	//Activity

	//Have a fetch API coed and specify what HTTP method to be 

	fetch("https://jsonplaceholder.typicode.com/posts/1",{
			//specify what HTTP method to be used
			method: 'DELETE',
			// body:JSON.stringify({
			// 	id: document.querySelector('#txt-edit-id').value,
			// 	title: document.querySelector('#txt-edit-title').value,
			// 	body: document.querySelector('#txt-edit-body').value,
			// 	userId:1
			// }),
			// headers: {'Content-type' : 'application/json ; charset=UTF-8'}
		})



	const deletePost=(id) =>{
		document.querySelector(`#post-${id}`).remove();
	}